//temporary create game button for testing purposes
function create_game() {
	var game_data = {
		'time':{'day':900,'night':120},
		'options':[],
		'catagories':{
			'town':[2, 'townsfolk', 'townsfolk'],
			'mafia':[1, 'mafioso'],
			'pr':[1, 'pr']
		},
		'roles':{
			'pr':['gun','townsfolk'],
			'mafioso':['mafioso'],
			'townsfolk': ['townsfolk']
		}
	};
	$.post('/creategame', game_data, function (data) {
		if (data && data !== 'debug-1') {
			window.location.href = '/game/'+data;
		} else {

		}
	});
}

function play_button(rank) {
	//do interface things here.. like a pre-pre-game screen
	$.get('/play/'+rank, function (data) {
		window.location.href = '/game/'+data.game_id+'?join_code='+data.join_code;
	});
}

