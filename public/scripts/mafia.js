var cardIcon = {'watch':'binoculars','seer':'eye-open','save':'heartbeat','follow':'shoe-steps','investigate':'search',
				'roleblock':'stop-sign','vigilante':'bullets','bodyguard':'police',
				'convert':'magic','steal':'magnet','hack':'laptop','gravedigger':'spade',
				'framer':'sunglasses','lawyer':'scale-classic','oracle':'meditation',
				'ironman':'veteran','bulletproof':'shield','mason':'group-chat','bomb':'bomb','godfather':'king','stealth':'cat',
				'odd nights':'dice3','even nights':'dice-2','one use':'flash','must act':'rabbit','noreveal':'eye-close','alert':'cardio',
				'slow':'turtle','lone wolf':'dog','one per night':'calendar','all in one':'bowling',
				'captain':'tower','soldier':'pawn','townsfolk':'temple-christianity-church','mafioso':'tie','suspicious':'user-alert',
				'others':'mosquito','gun':'riflescope','mayor':'crown','knife':'ax', 'associate':'link'};

var cardDesc = {'watch':'Learn the identities of each player who also paid your target a visit.',
				'seer':'Learn the full role of your target.',
				'save':'Your target will not die on any night this card is used.',
				'follow':'Learn the identities of each player your target pays a visit to on any night this card is used.',
				'investigate':'Learn which side your target is.',
				'roleblock':'Your target does not use any visiting actions on any night this card is used. Mafia roleblockers go first.',
				'vigilante':'Attempt to kill your target. Not exclusive with the mafia kill.',
				'bodyguard':'Visit your target, and if they would be killed that night, redirect the kill attempt to you.',
				'convert':'Your target will be on your side at the end of the night.',
				'steal':'Steal any and all non-alignment cards your target has.',
				'hack':'You select two targets. The night actions of the first selected target will be directed at the second instead.',
				'gravedigger':'Visit your target, and if they die that night their role will not be revealed.',
				'framer':'Your target will appear to be sided with the mafioso if checked on any night this card is used.',
				'lawyer':'Your target will appear to be sided with the townsfolk if checked on any night this card is used.',
				'oracle':'Visit your target, and ANY time you die the player you last targeted with this card will be revealed to everyone.',
				'ironman':'You cannot die at night or by attacks during the day.',
				'bulletproof':'Any attempt to kill you during the night or day will fail, but you will lose this card.',
				'mason':'If there are other players with the mason card, you may chat with them during the night.',
				'bomb':'Any attempts to kill you at night or during the day with weapons will also kill the attacker.',
				'godfather':'You appear to be sided with the townsfolk.',
				'stealth':'Immune to the effects of bomb, watch, follow, roleblock, and bodyguard.',
				'odd nights':'You may not use action cards on even-numbered nights (2, 4, 6, ..)',
				'even nights':'You may not use action cards on odd-numbered nights (1, 3, 5, ..)',
				'one use':'Once you use an action card, you lose that card.',
				'must act':'You cannot stay low on any night for any of your action cards. If you fail to pick someone it will be random.',
				'noreveal':'When you die, your role will not be revealed to everyone.',
				'alert':'cardio',
				'slow':'You cannot use night action cards while you have this card. One of these is removed after each night.',
				'lone wolf':'You do not participate in any night chats, or any night votes.*',
				'one per night':'You may only use one night action card per night.',
				'all in one':'Your target will be the same across all night action cards each night.',
				'captain':'Captains shall not be randomly selected to visit the mafia kill target unless only captains remain among the mafia.',
				'soldier':'If a soldier is alive, they will be top priority to be randomly selected to visit the mafia kill target.',
				'townsfolk':'You are sided with the townsfolk, your job is to work with the other townsfolk to identify and eliminate all mafioso and others.',
				'mafioso':'You are sided with the mafioso, your job is to hide and kill the townsfolk and others until they no longer outnumber you.',
				'others':'You are sided with the others, your job is to hide and kill the townsfolk and mafioso until they no longer outnumber you.',
				'suspicious':'You appear to be sided with the townsfolk. Any player with this card will not see that they have this card.',
				'gun':'Pick someone during the day and instantly make an attempt to kill them. Your identity as the shooter will be revealed. You lose this card upon use.',
				'mayor':'You may activate this at any point during the day to reveal your own identity as the mayor.',
				'knife':'Pick someone during the day and instantly make an attempt to kill them. Your identity will not be revealed. You lose this card upon use.',
				'associate':'You do not count toward your side in determining whether the mafia / others have majority and win.'};

//card classifications
var alignment_cards = ['townsfolk','mafioso','others'];
var dayactions = ['gun','mayor','knife'];
var nightactions = ['seer','save','follow','watch','investigate','roleblock','vigilante','bodyguard','convert','steal','hack',
					'framer','lawyer','oracle','gravedigger'];
var passivecards = ['ironman','bulletproof','mason','bomb','godfather','stealth'];
var modifiercards = ['odd nights','even nights','one use','must act','noreveal','nervous','slow','lone wolf','one per night','all in one',
				     'captain','soldier'];
var hiddencards = ['suspicious'];
var winconcards = ['associate'];

//current page for page scrolling
var current_page = game.page;

//is the setup daystart (quick page scrolling)
var daystart = (game.setup.options.indexOf('daystart') !== -1);

//format game seconds into a time string
function format_game_time(t) {
	var s = "00" + (t % 60);
	s = s.substr(s.length - 2);
	return Math.floor(t / 60).toString() + ':' + s;
}

function hasCard(card) {
	return (game.users[username].cards.indexOf(card) !== -1);
}
function hasOption (option) {
	return (game.setup.options.indexOf(option) !== -1);
}

isAllowedToAct = function () {
	if (hasCard('slow')) {return false;}
	if (game.phase === 'night') {
		if (hasCard('even nights') && Math.floor((game.page - Number(game.setup.options.indexOf('daystart') !== -1)) / 2) % 2 === 0) {return false;}
		if (hasCard('odd nights') && Math.floor((game.page - Number(game.setup.options.indexOf('daystart') !== -1)) / 2) % 2 === 1) {return false;}
		if (hasCard('one per night') && Object.keys(game.users[username].acting).length !== 0) {return false;}
	}
	return true;
}

function kill_user(data) {
	var user = data.user;
	
	//killing user in spirit
	game.users[user].alive = false;
	
	//user plaques now say "dead" and classify as "dead"
	$('[data="'+user+'"].user-plaque .voting-text').css({ 'color': 'red'});
	$('[data="'+user+'"].user-plaque .voting-text').text('dead');
	$('[data="'+user+'"].user-plaque').addClass('dead');
	
	//UNFINISHED(unvote in game data and living char vote-text) make sure no one's voting the dead user, and dead user can't be voted
	$('[data="'+user+'"].user-plaque .vote-button').removeClass('voted');
	$('[data="'+user+'"].user-plaque .vote-button').removeClass('voteable');
	
	//move desktop plaque to the graveyard
	$('[data="'+user+'"].desktop-user-plaque').insertAfter('#desktop-graveyard-marker');
	
	//move mobile plaque to the graveyard
	$('[data="'+user+'"].mobile-user-plaque').appendTo('#mobile-playerbox');
	
	//remove action targeting for the user
	$('#card-targeting-outer [data="'+user+'"]').remove();
	
}

function increment_phase(phase) {
	
	game.phase = phase;
	game.page += 1;
	game.messages[game.page] = [];
	game.users[username].acting = {};
	if (current_page === game.page - 1) {
		$('.chat-row').remove();
		current_page = game.page;
		day_or_night_display(current_page);
	}
	
	//resetting all votes client side (this always happens)
	for (var user in game.users) {
		game.users[user].vote = '';
		$('[data="'+user+'"].user-plaque .vote-button').removeClass('voted');
		if (game.users[user].alive) {
			$('[data="'+user+'"].user-plaque .voting-text').css({ 'color': 'rgb(200, 200, 200)'});
			$('[data="'+user+'"].user-plaque .voting-text').text('not voting');
			$('[data="'+user+'"].user-plaque .vote-button').addClass('voteable');
		}
	}
	
	//resetting all action picks visually (data is reset above)
	$("#action_pane").modal('hide');
	$('#card-targeting-outer tr').removeClass('action-picked');
	
}

//sets the day/night icon and number based on the page number
function day_or_night_display(page) {
	var timeofgame = document.getElementById('timeofgame');
	var adjust = 0;
	if (daystart && page > 0) {
		adjust = 1;
	}
	if (page === 0) {
		$('.timeofgame span').attr("class",'glyphicons glyphicons-clock');
		$('.timeofgame sub').text('');
	} else if ((page + adjust) % 2 === 0) {
		$('.timeofgame span').attr("class",'glyphicons glyphicons-sun');
		$('.timeofgame sub').text(Math.ceil(page / 2));
	} else {
		$('.timeofgame span').attr("class",'glyphicons glyphicons-moon');
		$('.timeofgame sub').text(Math.ceil(page / 2));
	}
}

function construct_action_button(card, alignment) {
	var actionbtn = document.createElement('div');
	actionbtn.className = 'action-btn pull-right '+alignment;
	var glyphicon = document.createElement('span');
	glyphicon.className = 'glyphicons glyphicons-'+cardIcon[card];
	actionbtn.setAttribute('onclick','open_action_pane("'+card+'");');
	actionbtn.appendChild(glyphicon);
	
	return actionbtn;
}

//constructing the easy-reference roleInfo variable.. who cares where really
var roleInfo = {'':{icon:'', align:''}};
for (var role in game.setup.roles) {
	roleInfo[role] = {};
	for (var i in game.setup.roles[role]) {
		if (alignment_cards.indexOf(game.setup.roles[role][i]) !== -1) {
			roleInfo[role].align = game.setup.roles[role][i];
		}
		if (!roleInfo[role].icon) {roleInfo[role].icon = game.setup.roles[role][i];}
	}
	//roleInfo[role].icon
}

function role_indicator(user, card, align) {
	for (var i in alignment_cards) {$('[data="'+user+'"].user-plaque').removeClass(alignment_cards[i]);}
	if (align !== '') {
		$('[data="'+user+'"].user-plaque').addClass(align);
	}
	if (card !== '') {
		$('[data="'+user+'"].user-plaque:not(:has(.role-indicator))').append('<span class="glyphicons glyphicons-'+cardIcon[card]+' role-indicator"></span>');
		$('[data="'+user+'"].user-plaque > .role-indicator').attr('class', 'glyphicons glyphicons-'+cardIcon[card]+' role-indicator');
	} else {$('[data="'+user+'"].user-plaque > .role-indicator').remove();}
}

function action_button_arrange() {
	
	var cards = game.users[username].cards;
	var alignment = game.users[username].alignment;
	
	if (cards) {
		
		var game_header = document.getElementById('game-header');
		if (cards.length > 3) {
			
			//add expansion button
			$('.expansion-btn').addClass(alignment);
			$('.expansion-btn').show();
			
			//add visible buttons
			game_header.appendChild(construct_action_button(cards[0], alignment));
			game_header.appendChild(construct_action_button(cards[1], alignment));
			
			//add hidden buttons
			var extra_button_zone = document.getElementById('extra-actnbtns');
			for (var i in cards) {if (i > 1) {
				extra_button_zone.appendChild(construct_action_button(cards[i], alignment));
			}}
			
			
			
		} else {
			//add visible buttons
			for (var i in cards) {
				game_header.appendChild(construct_action_button(cards[i], alignment));
			}
		}
		
	} 
}

//UNFINISHED
function construct_player_panel(u) {
	
	//deciding what the status text will say and what color it will be based on game data
	var statusText, statusStyle
	var vote_class = ' ';
	var dead_class = '';
	if (!game.users[u].alive) {
		statusText = 'dead';
		statusStyle = 'color:red;';
		dead_class = ' dead';
	} else if (game.phase !== 'pregame') {
		if ((hasCard('mafioso') && game.phase === 'night') || game.phase === 'day') {
			vote_class += 'voteable ';
		}
		if (game.users[u].vote === '') {
			statusText = 'not voting';
			statusStyle = 'color:rgb(200, 200, 200);';
			
		} else {
			statusText = game.users[u].vote;
			statusStyle = 'color:darkred;';
		}
	} else {
		statusText = 'not voting';
		statusStyle = 'color:rgb(200, 200, 200);';
	}
	if (game.users[username].vote === u) {vote_class += 'voted';}
	
	//setting up the HTML to insert when a player joins .. this code is probably a temp solution
	var HTML = '-user-plaque user-plaque'+dead_class+'" data="'+u+'"><table><tr><td rowspan="2">' +
	'<img src="/images/noavi.png" class="img-circle vote-button'+vote_class+'" width="30" height="30" onclick="icon_click(this, '+"'"+u+"'"+');">' +
	'</td><td><span class="name-text">'+u+'</span></td></tr><tr><td>' +
	'<span class="voting-text" style="'+statusStyle+'">'+statusText+'</span></td></tr></table></div>';
	
	
	if (dead_class === '') {
		$('#desktop-playerlist').prepend('<div class="row desktop'+ HTML);
		$('#mobile-playerbox').prepend('<div class="col-xs-4 mobile'+ HTML);
	}
	else {
		$('#desktop-playerlist').append('<div class="row desktop'+ HTML);
		$('#mobile-playerbox').append('<div class="col-xs-4 mobile'+ HTML);
	}

	//handle reveal
	if (game.users[u].reveal) {role_indicator(u, roleInfo[game.users[u].reveal].icon, roleInfo[game.users[u].reveal].align);}
	
}

function display_message(data) {
	
	//if the player is looking at the current page
	if (current_page === game.page) {
	
		var msgbox = document.getElementById('game-chat-box');
		var msgbox_inner = document.getElementById('game-chat-box-inner');
		
		//constructing the message element
		var msg = construct_msg_display(data);
		
		//checking to see if the scrollbar is at the bottom of the page
		var isScrolledToBottom = msgbox.scrollHeight - msgbox.clientHeight <= msgbox.scrollTop + 1;
		
		//actually adding in the message element
		msgbox_inner.appendChild(msg);
		
		//keeping the scrollbar where it was
		if (isScrolledToBottom) {
			msgbox.scrollTop = msgbox.scrollHeight - msgbox.clientHeight;
		}
	}
}

function construct_msg_display(data) {
	
	var chat_row = document.createElement("tr");
	chat_row.className = "chat-row";

	var msg = document.createElement("td");
	msg.className = "msg";
	var msgText = document.createTextNode(data.m);
	
	if (!data.u) {
		msg.setAttribute('colspan',3);
		chat_row.className += ' sysmsg';
	} else {
		data.avi = '/images/noavi.png';//Change this once you get custom avatars in play
		var avicol = document.createElement("td");
		avicol.className = "avi-img";
		var avi = document.createElement("img");
		avi.className = "img-circle";
		avi.src = data.avi; 
		avicol.appendChild(avi);
		chat_row.appendChild(avicol);
		
		var d_chatter_name = document.createElement("td");
		var m_chatter_name = document.createElement("span");
		d_chatter_name.className = "chatter-name-desktop";
		m_chatter_name.className = "chatter-name-mobile";
		var d_chatter_nameText = document.createTextNode(data.u);
		var m_chatter_nameText = document.createTextNode(data.u);
		d_chatter_name.appendChild(d_chatter_nameText);
		m_chatter_name.appendChild(m_chatter_nameText);
		chat_row.appendChild(d_chatter_name);

		msg.appendChild(m_chatter_name);
		chat_row.setAttribute('data',data.u);
	}
	msg.appendChild(msgText);
	chat_row.appendChild(msg);
	
	
	return chat_row;

}

function construct_action_module(data) {
	
	for (var user in data.users) {

		if (user !== username && data.users[user].alive) {
			var targeting_row = document.createElement("tr");
			
			var targeting_image = document.createElement("td");
			targeting_image.className = 'targeting-image';
			var avi = document.createElement("img");
			avi.className = "img-circle";
			avi.src = '/images/noavi.png';
			targeting_image.appendChild(avi);
			targeting_row.appendChild(targeting_image);
			
			var targeting_name = document.createElement("td");
			targeting_name.className = 'targeting-name';
			targeting_name.textContent = user;
			targeting_row.appendChild(targeting_name);
			
			$(targeting_row).attr('data', user)
			$(targeting_row).attr('onclick','action_pick(this);');
			$('#card-targeting-outer tbody').prepend(targeting_row);
		}
	}
}

//used to render a certain page
function render_page(page) {
	
	if (page <= game.page && page > -1) {
		
		//change icon displays
		day_or_night_display(page);
		
		//remove the contents of the old page
		$('.chat-row').remove();
	
		//rendering messages for current page
		var p = page;
		var msgbox_inner = document.getElementById('game-chat-box-inner');
		for (var i in game.messages[p]) {
			msgbox_inner.appendChild(construct_msg_display(game.messages[p][i]));
		}
		
		//set the current page being viewed by the client (not necessarily the last page)
		current_page = page;
		
		//scrolling to top
		document.getElementById('game-chat-box').scrollTop = 0;
	
	}
}

//used to render game history data passed by the server
function render_game_data(data) {
	
	//rendering user panes
	for (var user in data.users) {
		construct_player_panel(user);
	}
	
	day_or_night_display(game.page);
	action_button_arrange();
	if (data.phase !== 'pregame') {construct_action_module(data);}
	
	//rendering messages for current page
	var p = data.page;
	var msgbox_inner = document.getElementById('game-chat-box-inner');
	for (var i in data.messages[p]) {
		msgbox_inner.appendChild(construct_msg_display(data.messages[p][i]));
	}
	
	//scrolling to bottom
	var msgbox = document.getElementById('game-chat-box');
	msgbox.scrollTop = msgbox.scrollHeight - msgbox.clientHeight;
}

function construct_setup_display(setup) {
	var lines = [];//make a list for each text line to be printed
	lines.push("This setup is "+setup.options.join(", ")+".");
	for (catagory in setup.catagories) {
		var role_count = {};
		if (setup.catagories[catagory][0] === len(setup.catagories[catagory]) - 1) {
			//catagory can be treated as a list of single roles
			for (var i in setup.catagories[catagory]) {
				if (i > 0) {
					if (role_count.hasOwnProperty(setup.catagories[catagory][i])) {
						role_count[setup.catagories[catagory][i]] += 1;
					} else {role_count[setup.catagories[catagory][i]] = 1;}
				}
			}
			for (role in role_count) {
				lines.push(role_count.toString()+" "+role);//ADD IN CARD ICONS
			}
		} else {
			lines.push(setup.catagories[catagory][0].toString()+" "+catagory+" picked from...");
			//catagory must be treated as a catagory
			for (var i in setup.catagories[catagory]) {
				if (i > 0) {
					if (role_count.hasOwnProperty(setup.catagories[catagory][i])) {
						role_count[setup.catagories[catagory][i]] += 1;
					} else {role_count[setup.catagories[catagory][i]] = 1;}
				}
			}
			for (role in role_count) {
				lines.push("    "+role_count.toString()+" "+role);//ADD IN CARD ICONS
			}
		}
	}
}

render_game_data(game);

//set event handlers
//close action modal if transperant background is clicked (scrappy code)
$('#inner-action-pane').on('click', function (e) {
	if (e.target.className === 'row nospacing' || e.target.className === 'col-sm-6 nospacing') {
		$("#action_pane").modal('toggle');
	}
})



//invoke the socket.io client library
var socket = io();

//sending token to game server to establish socket
socket.emit('game handshake', {token: token});

//when a player clicks the icon of an avatar..
function icon_click(ele, voted_user) {
	
	//if it's day or if the player is mafia/others and it's night
	if (game.phase  === 'day' || (game.phase === 'night' && !hasCard('townsfolk'))) {

		//as long as it's not slow night for mafia
		if (!(hasOption('slow mafia') && game.page < 3)) {

			//if the player is already voted, unvote them
			if (game.users[username].vote === voted_user) {
				socket.emit('vote', {token: token, target: ''});
			}
			//otherwise unvote everyone else and vote that player
			else {
				socket.emit('vote', {token: token, target: voted_user});
			}
		}
	}
}

function open_action_pane(card) {

	//decide whether or not to show the targeting menu
	if (isAllowedToAct()) {
		if ((game.phase === 'night' && nightactions.indexOf(card) !== -1) || 
			(game.phase === 'day' && dayactions.indexOf(card) !== -1)) {$('#card-targeting-outer').show();}
		else {$('#card-targeting-outer').hide();}
	} else {$('#card-targeting-outer').hide();}

	//decide whether or not to show 'stay low'
	if (hasCard('must act') || dayactions.indexOf(card) !== -1) {
		$('#card-targeting-outer [data="stay low"]').appendTo('#hidden-bin');
	} else {
		$('#card-targeting-outer [data="stay low"]').appendTo('#card-targeting-outer tbody');
	}

	//handle single pick cards (mayor)
	if (card === 'mayor') {

	}
	
	//add the card data to the physical card
	$('#card-art-outer').attr('data', card);
	$('#card-art-topline').text(card);
	$('#card-art-description').text(cardDesc[card]);
	$('#card-art-icon').attr('class', 'glyphicons glyphicons-'+cardIcon[card]);
	
	//highlight current target if there is one
	if (game.users[username].acting[card]) {
		var target = game.users[username].acting[card];
		
		$('#card-targeting-outer tr').removeClass('action-picked');
		$('#card-targeting-outer [data="'+target+'"]').addClass('action-picked');
	}
	
	//or clean it up if there isn't
	else {
		$('#card-targeting-outer tr').removeClass('action-picked');
	}
	
	//check to see if acting is disabled
	
	$("#action_pane").modal({
		'keyboard': true,
		'focus': true
	});
}

function action_pick(ele) {
	var target = $(ele).attr('data');
	var card = $('#card-art-outer').attr('data');
	socket.emit('action', {token: token, card: card, target: target});
}

function enter_key_msg(event) {
	
	//if the player pressing enter
	if (event.which === 13 || event.keyCode === 13) {
		send_msg();
	}
}
function send_msg() {
	var message = $('#message-input')[0].value;
	
	//if the player has something typed already..
	if (message !== "") {
		socket.emit('msg', {token: token, content: message});
		$('#message-input')[0].value = "";
	}
}
function quote(user, message, page) {
	if (message !== "" && user !== "") {
		socket.emit('msg', {token: token, content: message, quote: user, page: page});
	}
}

function toggle_mobile(v) {

	if (v === 0) {var hide_class = 'hide-graveyard';}
	else if (v === 1) {
		var hide_class = 'hide-all';
		var ele = $('#hide-players-mobile-button span')[0];
	}

	//check location of scrollbar
	var msgbox = document.getElementById('game-chat-box');
	var isScrolledToBottom = msgbox.scrollHeight - msgbox.clientHeight <= msgbox.scrollTop + 1;

	if ($('#mobile-playerbox').hasClass(hide_class)) {
		if (ele) {ele.className = 'glyphicons glyphicons-minus';}
		$('#mobile-playerbox').removeClass(hide_class);
	} else {
		if (ele) {ele.className = 'glyphicons glyphicons-plus';}
		$('#mobile-playerbox').addClass(hide_class);
	}

	//keeping the scrollbar where it was
	if (isScrolledToBottom) {
		msgbox.scrollTop = msgbox.scrollHeight - msgbox.clientHeight;
	}
}
keyboard_shown = 0;//global var for this
function toggle_keyboard() {

	//check location of scrollbar
	var msgbox = document.getElementById('game-chat-box');
	var isScrolledToBottom = msgbox.scrollHeight - msgbox.clientHeight <= msgbox.scrollTop + 1;

	if (keyboard_shown === 0) {
		keyboard_shown = 1;
		$('#mobile-playerbox-outer').hide();
	} else {
		keyboard_shown = 0;
		$('#mobile-playerbox-outer').show();
	}

	//keeping the scrollbar where it was
	if (isScrolledToBottom) {
		msgbox.scrollTop = msgbox.scrollHeight - msgbox.clientHeight;
	}
}

/////////////////////////////// WEBSOCKET EVENTS

//when the user recieves notification that someone has joined the game .. Logic not set up for subs just yet
socket.on('user joined game', function (data) {
	
	//adding to game data
	game.users[data.username] = data.userdata;
	
	//constructing panel for user
	construct_player_panel(data.username);

	game.messages[game.page].push(data.message);
	display_message(data.message);
	
	//if the game is full, notify that it'll start soon
	if (Object.keys(game.users).length === game.total_players) {
		game.timer = Date.now() + 5000;
		update_timer();
	} else {
		$('#game-status-ticker').text(String(game.total_players - Object.keys(game.users).length)+' spots left.');
	}
});

//when the user recieves notification that someone has voted
socket.on('vote change', function (data) {
	
	//if the vote is taken off then it's grey
	if (data.voted === '') {
		game.users[data.voter].vote = '';
		if (data.voter === username) {$('.voted').removeClass('voted');}
		$('[data="'+data.voter+'"] .voting-text').css('color', 'rgb(200, 200, 200)');
		$('[data="'+data.voter+'"] .voting-text').each(function () {this.innerHTML = 'not voting';});
		
	}
	//if the player is voting the color is red
	else {
		game.users[data.voter].vote = data.voted;
		
		if (data.voter === username) {
			$('.voted').removeClass('voted');
			$('[data="'+data.voted+'"] .vote-button').addClass('voted');}
		$('[data="'+data.voter+'"] .voting-text').css('color', 'darkred');
		$('[data="'+data.voter+'"] .voting-text').each(function () {this.innerHTML = ''+data.voted;});
	}
	game.messages[game.page].push(data.message);
	display_message(data.message);
	
});

//when the user recieves confirmation that their action target has switched
socket.on('action confirmed', function (data) {
	
	//if the target is taken off remove the highlight
	if (game.users[username].acting[data.card] && data.target === game.users[username].acting[data.card]) {
		delete game.users[username].acting[data.card];
		$('#card-targeting-outer tr').removeClass('action-picked');
	}
	//if the target is someone new, or stay low
	else {
		game.users[username].acting[data.card] = data.target;
		$('#card-targeting-outer tr').removeClass('action-picked');
		$('#card-targeting-outer [data="'+data.target+'"]').addClass('action-picked');
	}
	
	game.messages[game.page].push(data.message);
	display_message(data.message);
	
});

//when the user gets notification that the game has started, and learns its roles etc
socket.on('game start', function (data) {
	game.timer = Date.now() + data.timer * 1000;
	update_timer()
	game.users[username].role = data.role;
	game.users[username].cards = data.cards;
	if (data.cards.indexOf('townsfolk') !== -1) {game.users[username].alignment = 'townsfolk';}
	else if (data.cards.indexOf('mafioso') !== -1) {game.users[username].alignment = 'mafioso';}
	else {game.users[username].alignment = 'others';}
	action_button_arrange();
	increment_phase(data.phase);
	construct_action_module(game)
	
	game.messages[game.page].push(data.message);
	display_message(data.message);
	
});

//when a day ends
socket.on('day end', function (data) {
	game.timer = Date.now() + data.timer * 1000;
	update_timer()
	increment_phase('night');
	
	if (data.lynched !== 'no one') {
		kill_user({user: data.lynched});
		game.users[data.lynched].reveal = data.role;
		role_indicator(data.lynched, roleInfo[data.role].icon, roleInfo[data.role].align);
	}
	
	game.messages[game.page].push(data.message);
	display_message(data.message);
});

//when a night ends
socket.on('night end', function (data) {
	game.timer = Date.now() + data['@timer'] * 1000;
	update_timer()
	delete data['@timer'];
	increment_phase('day');
	
	for (var victim in data) {
		kill_user({user: victim});
		game.users[victim].reveal = data[victim][1];
		role_indicator(victim, roleInfo[data[victim][1]].icon, roleInfo[data[victim][1]].align);
		game.messages[game.page].push(data[victim][0]);
		display_message(data[victim][0]);
	}
	
});

//update timer during the middle of the day
socket.on('update timer', function (data) {
	game.timer = Date.now() + data * 1000;
	update_timer();
});

//day kill
socket.on('day kill', function (data) {
	
	kill_user({user: data.target});
	game.users[data.target].reveal = data.role;
	role_indicator(data.target, roleInfo[data.role].icon, roleInfo[data.role].align);
	game.messages[game.page].push(data.message);
	display_message(data.message);
	
});

//when the user gets an ingame message from another player via websocket
socket.on('msg', function (data) {
	game.messages[game.page].push(data);
	display_message(data);
});

//player is revealed!
socket.on('reveal', function (data) {
	game.users[data.user].reveal = data.role;
	role_indicator(data.user, roleInfo[data.role].icon, roleInfo[data.role].align);
});

//card is removed from this player
socket.on('remove card', function (data) {
	var array = game.users[username].cards;
	var index = array.indexOf(data.card);
	if (index > -1) {array.splice(index, 1);}
	$('#extra-actnbtns, .action-btn, .expansion-btn').remove();
	action_button_arrange();
});

//new card is added to this player
socket.on('add card', function (data) {
	game.users[username].cards.push(card);
	$('#extra-actnbtns, .action-btn, .expansion-btn').remove();
	action_button_arrange();
});

//time management
function update_timer() {
	if (game.timer) {
		var num, str;
		var ms = Math.max(game.timer - Date.now(), 0);
		if (ms > 90000) {num = Math.ceil(ms / 60000);str= num === 1 ? 'minute' : 'minutes'}
		else {num = Math.ceil(ms / 1000);str= num === 1 ? 'second' : 'seconds'}

		$('#game-status-ticker').text(String(num) + ' ' + str + ' left..');
	}
}
//starting main loop
var main_loop = setInterval(function () {
	update_timer();
}, 1000);
//setting initial ticker message & timer
if (!game.timer) {
	$('#game-status-ticker').text(String(game.total_players - Object.keys(game.users).length)+' spots left.');
} else {game.timer = game.timer + Date.now();update_timer();}

////////////////////////// EVENTS //////////////////////////

