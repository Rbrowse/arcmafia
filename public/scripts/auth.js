function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function email_form_valid(s) {
	var problem_response = false;
	var sides = s.split('@');
	if (sides.length === 2) {
		if (sides[0].length > 0 && sides[1].length > 2) {
			for (c in s.toLowerCase()) {
				if ('qwertyuiopasdfghjklzxcvbnm1234567890-_.@'.indexOf(s.toLowerCase()[c]) === -1) {
					problem_response = true;
				}
			}
			trail_sides = sides[1].split('.')
			if (trail_sides.length === 2) {
				for (c in sides[1].toLowerCase()) {
					if ('qwertyuiopasdfghjklzxcvbnm1234567890.'.indexOf(sides[1].toLowerCase()[c]) === -1) {
						problem_response = true;
					}
				}
			} else {problem_response = true;}
			if (!problem_response) {
				return true
			}
		}
	}
	return false
}

// create alert in the authentication window
function auth_alert(al_type, al_strong, al_body) {
	var old_alerts = $('#register-warning-box div');
	if (old_alerts.length > 1) {old_alerts[0].remove();}
	var new_alert = '<div class="alert alert-'+al_type+'"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'+al_strong+'</strong>'+al_body+'</div>';
	$('#register-warning-box').append(new_alert);
}

// when you deselect the username field
function desel_usr(ele) {
	var username = ele.value;
	if (username.length > 0) {
		var problem_response = false;
		for (c in username.toLowerCase()) {
			if ('qwertyuiopasdfghjklzxcvbnm1234567890-_.'.indexOf(username.toLowerCase()[c]) === -1) {
				problem_response = true;
			}
		}
		if (problem_response) {auth_alert('danger','Username',' contains invalid characters. Only use numbers, letters, ., -, and _.');}
		if (username.length < 4) {auth_alert('danger','Username',' must be at least 4 characters.');problem_response = true;}
		if (username.length > 16) {auth_alert('danger','Username',' maximum length is 16 characters.');problem_response = true;}
		if (problem_response) {
			ele.parentElement.className += ' has-error';
			ele.nextSibling.nextSibling.className += ' glyphicon-remove';
		} else {
			$.post('/check_username', {'username' : username}, function (data) {
				if (data === 'available') {
					ele.parentElement.className += ' has-success';
					ele.nextSibling.nextSibling.className += ' glyphicon-ok';
				} else if (data === 'taken') {
					auth_alert('danger','Username',' is taken!');
					ele.parentElement.className += ' has-error';
					ele.nextSibling.nextSibling.className += ' glyphicon-remove';
				}
			});
		}
	}
}

// when you deselect the password field
function desel_pwd(ele) {
	var pass = ele.value;
	if (pass.length > 0) {
		var problem_response = false;
		for (c in pass.toLowerCase()) {
			if ('qwertyuiopasdfghjklzxcvbnm1234567890,?=+(){}[]<>?/|*:;~`!@#$%^& -_.'.indexOf(pass.toLowerCase()[c]) === -1) {
				problem_response = true;
			}
		}
		if (problem_response) {auth_alert('danger','Password',' contains invalid characters.');}
		if (pass.length < 6) {auth_alert('danger','Password',' must be at least 6 characters.');problem_response = true;}
		if (pass.length > 300) {auth_alert('danger','Password',' maximum length is 300 characters.');problem_response = true;}
		if (problem_response) {
			ele.parentElement.className += ' has-error';
			ele.nextSibling.nextSibling.className += ' glyphicon-remove';
		} else if (scorePassword(pass) < 60) {
			ele.parentElement.className += ' has-warning';
			ele.nextSibling.nextSibling.className += ' glyphicon-warning-sign';
			auth_alert('warning','Password',' is weak, consider using symbols, some capital, some lower case, some numbers, or a longer phrase.');
		} else {
			ele.parentElement.className += ' has-success';
			ele.nextSibling.nextSibling.className += ' glyphicon-ok';
		}
	}
}

// when you deselect the email field
function desel_email(ele) {
	var email = ele.value;
	if (email.length > 0) {
		var problem_response = false;
		if (!email_form_valid(email)) {auth_alert('danger','Email',' is not in valid format.');problem_response = true;}
		if (email.length > 60) {auth_alert('danger','Email',' maximum length is 60 characters.');problem_response = true;}
		if (problem_response) {
			ele.parentElement.className += ' has-error';
			ele.nextSibling.nextSibling.className += ' glyphicon-remove';
		} else {
			$.post('/check_email', {'email' : email}, function (data) {
				if (data === 'available') {
					ele.parentElement.className += ' has-success';
					ele.nextSibling.nextSibling.className += ' glyphicon-ok';
				} else if (data === 'taken') {
					auth_alert('danger','Email',' is already in use!');
					ele.parentElement.className += ' has-error';
					ele.nextSibling.nextSibling.className += ' glyphicon-remove';
				}
			});
		}
	}
}

// when you deselect the confirm password field
function desel_cfpwd(ele) {
	if (ele.value.length > 0) {
		if (document.getElementById('pwd').value === ele.value) {
			ele.parentElement.className += ' has-success';
			ele.nextSibling.nextSibling.className += ' glyphicon-ok';
		} else {
			ele.parentElement.className += ' has-error';
			ele.nextSibling.nextSibling.className += ' glyphicon-remove';
			auth_alert('danger','Passwords',' do not match!');
		}
	}
}

// when you select an input field
function sel_auth_input(ele) {
	if (ele.id === 'pwd') {
		sel_auth_input($('#cfpwd')[0]);
	}
	ele.parentElement.className = 'form-group has-feedback';
	ele.nextSibling.nextSibling.className = 'glyphicon form-control-feedback';
}

// submit the registration form
function complete_registration() {
	
	// check terms agreement
	if ($('#terms-agree')[0].checked) {
		
		// setting variables
		var email = $('#email')[0].value;
		var username = $('#usr')[0].value;
		var pass = $('#pwd')[0].value;
		var passconf = $('#cfpwd')[0].value;
		
		// checking if passwords match
		if (pass === passconf) {
			$.post('/register', {'email':email, 'username':username, 'pass':pass}, function (data) {
				if (data === '1') {
					window.location.href = '/';
				} else {
					auth_alert('danger','',data);
				}
			});
		} else {console.log('a')}
	} else {
		auth_alert('danger','','You must agree to the terms and privacy policy to register.');
	}
}

function complete_login() {
	var loginname = $('#loginname')[0].value;
	var pass = $('#loginpwd')[0].value;
	
	$.post('/login', {'loginname':loginname, 'pass':pass}, function (data) {
		if (data === '1') {
			window.location.href = '/';
		} else {
			var new_alert = '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong></strong>Username or password incorrect.</div>';
			$('#login-warning-box').append(new_alert);
		}
	});
}