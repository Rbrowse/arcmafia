
//packaged npm modules / node modules
express = require('express');
app = express();
http = require('http').Server(app);
expressSession = require('express-session');
cookieParser = require('cookie-parser');
bodyParser = require('body-parser');
mongoose = require('mongoose');

//modules to be packaged (npm)
io = require('socket.io')(http);
request = require("request");
validator = require("email-validator");
get_ip = require('ipware')().get_ip;
crypto = require('crypto');

//global custom modules
core = require('./custom_modules/general/functions.js');
schemas = require('./custom_modules/db/schemas.js');
models = require('./custom_modules/db/models.js');
//index only custom modules
var games = require('./custom_modules/game/gameserver.js');
var getRoutes = require('./custom_modules/routes/get.js');
var postRoutes = require('./custom_modules/routes/post.js');

//Cookie and body parser, sessions
app.use(cookieParser());
app.use(expressSession({
	secret: '90U8873j8f323HJD83GG',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(express.static('public'));

//set view engine to ejs
app.set('view engine', 'ejs');

//listen on port 80
http.listen(80, function() {
	console.log('listening on port 80');
});

//connect to db
mongoose.connect('mongodb://localhost/data/db', function(err) {
	if (err) {
		console.log("Error connecting to db: " + err);
	}
	else {
		console.log("Connection to db successful!");
	}
});

//run custom modules
getRoutes();
postRoutes();
games();

//catch errors
process.on('uncaughtException', function (err) {
	console.log(err);
});

handleError = function(err) {
	console.log(err);
}

