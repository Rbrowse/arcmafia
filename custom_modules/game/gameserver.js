/* global handleError */
/* global models */
/* global app */

module.exports = function () {
	
	//global serverSession (STR)
	//serverSession = core.random(5, 'abcdefghijklmnopqrstuwxyz0123456789');
	serverSession = 'debug';
	
	//global <setup options>
	//more cards: 'self visit', 'associate'
	allowed_options = ['daystart','must lynch','must kill','slow mafia','hardclaims'];
	alignment_cards = ['townsfolk','mafioso','others'];
	dayactions = ['gun','mayor','knife'];
	nightactions = ['seer','save','follow','watch','investigate','roleblock','vigilante','bodyguard','convert','steal','hack',
				    'framer','lawyer','oracle','gravedigger'];//mafioso is a special case not in here
	passivecards = ['ironman','bulletproof','mason','bomb','godfather','stealth'];
	modifiercards = ['odd nights','even nights','one use','must act','noreveal','nervous','slow','lone wolf','one per night','all in one',
					 'captain','soldier'];
	hiddencards = ['suspicious'];
	winconcards = ['associate'];
	allowed_cards = alignment_cards.concat(dayactions, nightactions, passivecards, modifiercards, hiddencards, winconcards);

	//setups TEMPOARARY
	var TEMP_setups = {};
	TEMP_setups['setup_1'] = {
		'time':{'day':900,'night':120},
		'options':[],
		'catagories':{
			'town':[2, 'townsfolk', 'townsfolk'],
			'mafia':[1, 'mafioso'],
			'pr':[1, 'pr']
		},
		'roles':{
			'pr':['gun','townsfolk'],
			'mafioso':['mafioso'],
			'townsfolk': ['townsfolk']
		}
	};
					 
	//global last_game_id (INT)
	last_game_id = 0;
	
	//global tokens {token(STR) : username(STR), ..}
	tokens = {};
	//global join_codes {join_code : {user:username, game_id:game_id}, ..}
	join_codes = {};
	
	//global gameusers {username(STR) : game_id(STR), ..}
	gameusers = {};
	
	//global games {game_id(STR) : {{username(STR) : {'votes' : {..}, X}, ..}, X}, ..}
	games = {};

	//global games_filling {'ranked' : [game_id, ..], 'casual' ; [], 'custom' : []}
	games_filling = {};
	games_filling.ranked = [];
	games_filling.casual = [];
	games_filling.custom = [];

	//global needs_sub {game_id : [username, ..]}
	needs_sub = {};
	
	//global usersockets
	usersockets = {};

	
	//convenience functions
	hasCard = function (username, card) {
		return (games[gameusers[username]].users[username].cards.indexOf(card) !== -1);
	}
	hasOption = function (game_id, option) {
		return (games[game_id].setup.options.indexOf(option) !== -1);
	}
	removeCard = function (username, card) {
		var game_id = gameusers[username];
		var array = games[game_id].users[username].cards;
		var index = array.indexOf(card);
		if (index > -1) {array.splice(index, 1);}
		io.to('@'+username).emit('remove card', {card: card});

		if (card === 'slow') {
			if (array.indexOf('slow') === -1) {
				io.to('@'+username).emit('msg', {m: 'You are no longer slow!', '@':[username]});
			}
		} else {
			io.to('@'+username).emit('msg', {m: 'You have lost your '+card+' card!', '@':[username]});
		}
	}
	addCard = function (username, card) {
		var game_id = gameusers[username];
		games[game_id].users[username].cards.push(card);
		io.to('@'+username).emit('add card', {card: card});
		io.to('@'+username).emit('msg', {m: 'You have gained the '+card+' card!', '@':[username]});
	}
	isAlive = function (username) {
		if (username !== '' && username !== 'stay low' && username !== 'no one') {return games[gameusers[username]].users[username].alive;} else {return false;}
	}
	alivePlayers = function (game_id, except) {
		var aliveList = [];
		for (var user in games[game_id].users) {
			if (isAlive(user) && user !== except) {
				aliveList.push(user);
			}
		}
		return aliveList;
	}

	isAllowedToAct = function (username) {
		var game_id = gameusers[username];

		if (hasCard(username, 'slow')) {return false;}
		if (games[game_id].phase === 'night') {
			if (hasCard(username, 'even nights') && Math.floor((games[game_id].page - Number(hasOption(game_id, 'daystart'))) / 2) % 2 === 0) {return false;}
			if (hasCard(username, 'odd nights') && Math.floor((games[game_id].page - Number(hasOption(game_id, 'daystart'))) / 2) % 2 === 1) {return false;}
			if (hasCard(username, 'one per night') && Object.keys(games[game_id].users[username].acting).length !== 0) {return false;}
		}
		return true;
	}
	reveal = function (username, role) {
		if (role !== '') {
			var game_id = gameusers[username];
			games[game_id].users[username].reveal = role;
			io.to(game_id).emit('reveal', {user: user, role: role});
		}
	}
	tallyVote = function (game_id) {
		var vote_count = {};
		var max_vote = 0;
		var voted_players = [];
		
		//tally each player's vote and set their vote to empty at the same time
		for (var user in games[game_id].users) {
			var vote = games[game_id].users[user].vote;
			games[game_id].users[user].vote = '';
			if (vote_count[vote]) {vote_count[vote] += 1;} else {vote_count[vote] = 1;}
			if (vote_count[vote] > max_vote && vote !== '') {max_vote = vote_count[vote];}
		}
		for (var user in vote_count) {
			if (vote_count[user] === max_vote && user !== '') {voted_players.push(user);}
		}

		
		//if one player has the most votes
		if (voted_players.length === 1) {
			//return that person
			return voted_players[0];
		//if multiple players have the most votes
		} else if (voted_players.length > 1) {
			//pick one at random
			return core.randomElement(voted_players);
		//if no one is voting.. no one gets voted, unless it's must lynch/kill, in which case you get a randokill
		} else {
			//lets flag games where this happens tho.. build that
			return 'no one';
		}
	}
	
	//function to package necessary game data to give to client
	client_game_data = function (game_id, username) {
		var client_data = {};
		
		//equivelant between server and client
		client_data.setup = games[game_id].setup;
		client_data.phase = games[game_id].phase;
		client_data.page = games[game_id].page;
		client_data.timer = games[game_id].endtime - Date.now();
		client_data.total_players = games[game_id].total_players;
		
		//messages the player is allowed to see
		client_data.messages = {};
		for (var page in games[game_id].messages) {
			client_data.messages[page] = [];
			var d = games[game_id].messages[page];
			for (var i in d) {
				if (!d[i]['@']) {
					client_data.messages[page].push(d[i]);
				} else if (d[i]['@'].indexOf(username) !== -1) {
					var msg = JSON.parse(JSON.stringify(d[i]));
					delete msg['@'];
					client_data.messages[page].push(msg);
				}
			}
		}
		
		//user info the player can see
		client_data.users = {};
		for (var user in games[game_id].users) {
			if (user === username) {
				client_data.users[user] = JSON.parse(JSON.stringify(games[game_id].users[user]));
				//hiding hidden cards
				var all_cards = client_data.users[user].cards;
				var known_cards = [];
				for (var card in all_cards) {
					if (hiddencards.indexOf(all_cards[card]) === -1) {
						known_cards.push(all_cards[card]);
					}
				}
				client_data.users[user].cards = known_cards;
			} else {
				client_data.users[user] = {};
				client_data.users[user].alive = games[game_id].users[user].alive;
				if (games[game_id].users[user].reveal) {
					client_data.users[user].reveal = games[game_id].users[user].reveal;
				}
				if (games[game_id].phase === 'day') {
					client_data.users[user].vote = games[game_id].users[user].vote;
				} else if (games[game_id].phase === 'night') {
					if (hasCard(user, 'mafioso') && hasCard(username, 'mafioso')) {
						client_data.users[user].vote = games[game_id].users[user].vote;
					} else if (hasCard(user, 'others') && hasCard(username, 'others')) {
						client_data.users[user].vote = games[game_id].users[user].vote;
					} else {
						client_data.users[user].vote = '';
					}
				}
			}
		}
		
		return client_data;
		
	};
	
	increment_phase = function (game_id) {
		
		//figure out which phase to set it to
		if (games[game_id].phase === 'pregame') {
			if (!hasOption(game_id, 'daystart')) {
				games[game_id].phase = 'night';
				games[game_id].events.endphase
			} else {
				games[game_id].phase = 'day';
			}
		} else if (games[game_id].phase === 'day') {
			games[game_id].phase = 'night';
		} else {
			games[game_id].phase = 'day';
		}
		
		//increment the page
		games[game_id].page += 1;
		
		//create a new message catagory for the current (new) page
		games[game_id].messages[games[game_id].page] = [];

	}
	force_phase = function (game_id) {

		//catalogue who's not voting
		var vegging_users = [];
		for (var user in games[game_id].users) {
			if (games[game_id].users[user].vote === '') {
				if (games[game_id].phase === 'day' || (hasCard(user, 'mafioso') && !(hasOption(game_id, 'slow mafia') && games[game_id].page < 3))) {
					vegging_users.push(user);
				}
			}
		}

		if (games[game_id].phase === 'night') {

			//make all actions that aren't picked 'stay low' .. just for must acts
			for (var user in games[game_id].users) {
				//check each of their cards
				for (var i in games[game_id].users[user].cards) {
					var card = games[game_id].users[user].cards[i];
					
					//if the card is a known night action and hasn't been decided on
					if (nightactions.indexOf(card) !== -1 && !games[game_id].users[user].acting.hasOwnProperty(card)) {
						games[game_id].users[user].acting = 'stay low';
					}
				}
			}
			end_night(game_id);
		} else {
			end_day(game_id);
		}
	}
	activateTimer = function (game_id, phase) {
		var seconds;
		if (isNaN(phase)) {
			seconds = games[game_id].setup.time[phase];
		} else {
			seconds = phase;
		}
		games[game_id].endtime = Date.now() + seconds * 1000;
		clearTimeout(games[game_id].events.endphase);
		games[game_id].events.endphase = setTimeout(function () {
			force_phase(game_id);
		}, seconds * 1000);
		return seconds;
	}
	handleVotes = function (game_id, playersNotVoting) {
		if (playersNotVoting === 1) {
			if (games[game_id].endtime > Date.now() + 60000) {
				games[game_id].slowtimerstorage = games[game_id].endtime;
				io.to(game_id).emit('update timer', activateTimer(game_id, 60));
			}
		} else if (playersNotVoting > 1) {
			if (games[game_id].slowtimerstorage) {
				if (games[game_id].slowtimerstorage > games[game_id].endtime) {
					var seconds = (games[game_id].slowtimerstorage - Date.now()) / 1000;
					io.to(game_id).emit('update timer', activateTimer(game_id, seconds));
				}
				delete games[game_id].slowtimerstorage;
			}
		} else {
			delete games[game_id].slowtimerstorage;
			end_day(game_id);
		}
	}
	
	//start the game itself
	startgame = function (game_id) {
		
		//confirm there's the right number of players
		if (games[game_id].total_players === Object.keys(games[game_id].users).length) {
			var rank = games[game_id].rank;
			create_game(get_a_setup(rank), rank);
			var index = games_filling[rank].indexOf(game_id);
			games_filling[rank].splice(index, 1);



			increment_phase(game_id);
			
			//organize roles into random ordered array
			var roleset = [];
			for (var catagory in games[game_id].setup.catagories) {
				var bin = games[game_id].setup.catagories[catagory].slice();
				var n = bin.shift();
				core.shuffleArray(bin);
				bin.length = n;
				roleset = roleset.concat(bin);
			} core.shuffleArray(roleset);
			
			//set timer here
			var timer = activateTimer(game_id, hasOption(game_id, 'daystart') ? 'day' : 'night');

			//actually setting the roles and sending the data
			var increment = 0;
			for (var user in games[game_id].users) {
				
				//getting roles and cards
				var role = roleset[increment];
				var cards = games[game_id].setup.roles[roleset[increment]].slice();
				
				//putting the user in its respective socket rooms and setting alignment variables
				if (cards.indexOf('mafioso') !== -1 ) {
					if (cards.indexOf('lone wolf') === -1) {usersockets[user].join(game_id+':mafia');}
					games[game_id].users[user].alignment = 'mafioso';
				}
				else if (cards.indexOf('others') !== -1) {
					if (cards.indexOf('lone wolf') === -1) {usersockets[user].join(game_id+':others');}
					games[game_id].users[user].alignment = 'others';
				} else {games[game_id].users[user].alignment = 'townsfolk';}
				if (cards.indexOf('mason') !== -1 && cards.indexOf('lone wolf') === -1) {usersockets[user].join(game_id+':mason');}
				
				//setting the user's role and cards in the globalvar
				games[game_id].users[user].role = role;
				games[game_id].users[user].cards = cards;
				
				increment++;//ready for the next random role
				
				//sending the player a message
				var message = 'Your role is: '+role+'. Your cards are: '+cards.join(", ")+'.';
				io.to('@'+user).emit('game start', {role:role, cards:cards, phase:games[game_id].phase, message:{'m':message}, timer:timer});
				games[game_id].messages[games[game_id].page].push({'m':message, '@':[user]});
			}
			
			//set the game start time
			//games[game_id].time.start = Math.floor(Date.now() / 1000);
		}
		//if there's a mismatch in the number of players for whatever reason..
		else {
			
		}
	};
	
	//check to see who wins, and execute game ending actions if someone wins
	check_wincons = function (game_id) {
		var mafia_count = 0;
		var town_count = 0;
		var other_count = 0;
		
		//check all players
		for (var user in games[game_id].users) {
			
			//if it's alive..
			if (games[game_id].users[user].alive) {
				
				//add it to whatever alignment count it is
				if (hasCard(user, 'mafioso')) {mafia_count += 1;}
				else if (hasCard(user, 'townsfolk')) {town_count += 1;}
				else if (hasCard(user, 'others')) {other_count += 1;}
				
			}
			
		}
		
		//if town wins
		if (mafia_count === 0 && other_count === 0) {
			return true;
		}
		//if mafia wins
		else if (mafia_count => town_count + other_count) {
			return true;
		}
		//if others win
		else if (other_count => town_count + mafia_count) {
			return true;
		}
		//if no one has won yet
		else {
			return false;
		}
		
	}
	
	////////////////////////////////////////////////////////////////////////////////
	//check to see if the night is ready to end. if it is, initiate night end
	check_night_end = function (game_id) {
		
		//check all players
		for (var user in games[game_id].users) {
			
			//if the player is alive
			if (isAlive(user)) {
				
				//if the player can act..
				if (isAllowedToAct(user)) {

					//check each of their cards
					for (var i in games[game_id].users[user].cards) {
						var card = games[game_id].users[user].cards[i];
						
						//if the card is a known night action and hasn't been decided on
						if (nightactions.indexOf(card) !== -1 && !games[game_id].users[user].acting.hasOwnProperty(card)) {
							return;
						}
					}
				}
				
				//if it's not a slow mafia night
				if (hasOption(game_id, 'slow mafia') || games[game_id].page > 2) {

					//and if they're mafia and haven't voted
					if (hasCard(user,'mafioso') && games[game_id].users[user].vote === '') {
						return;
					}
				}
			}
		}
		
		end_night(game_id);
	}
	
	//check to see if the day is ready to end. if it is, initiate day end
	check_day_end = function (game_id) {
		var requiredToEnd = alivePlayers(game_id).length;
		
		//check all players
		for (var user in games[game_id].users) {
			
			//if the player is alive and not voting
			if (isAlive(user) && games[game_id].users[user].vote !== '') {
				requiredToEnd -= 1;
			}
		}
		
		handleVotes(game_id, requiredToEnd);
	}
	
	//all the actions involved in the end of the day .. includes LYNCH KILL event
	end_day = function (game_id) {
		
		//identify the lynch target
		var lynch_target = tallyVote(game_id);

		//random lynch if it's must lynch and no one is selected
		if (lynch_target === 'no one' && hasOption(game_id, 'must lynch')) {
			var alive_targets = [];
			for (var user in users) {
				if (users[user].alive) {alive_targets.push(user);}
			}
			lynch_target = core.randomElement(alive_targets);
		}
		
		//increment phase
		increment_phase(game_id);
		
		//empty the action set in preparation for the night
		for (var user in games[game_id].users) {
			games[game_id].users[user].acting = {};
		}

		//kill the lynched target and set the message to send off
		if (lynch_target !== 'no one') {
			var message;
			var role = '';
			games[game_id].users[lynch_target].alive = false;
			if (hasCard(lynch_target, 'noreveal')) {
				message = {m: victim+' The day ends in the lynch of '+lynch_target+'!'};
			}
			else {
				role = games[game_id].users[lynch_target].role;
				games[game_id].users[lynch_target].reveal = role;
				message = {m:'The day ends in the lynch of '+lynch_target+', the '+role+'.'};
			}
		} else {
			var message = {m:'The day ends, no one is lynched.'};
		}
		
		
		games[game_id].messages[games[game_id].page].push(message);
		var timer = activateTimer(game_id, 'night');
		io.to(game_id).emit('day end', {lynched: lynch_target, message: message, role: role, timer: timer});
		check_wincons(game_id);
	}
	
	//all the actions involved in the end of the night
	end_night = function (game_id) {
		
		var users = games[game_id].users;	
		var night_target = tallyVote(game_id);

		//random kill if it's must kill and no one is selected
		if (night_target === 'no one' && hasOption(game_id, 'must kill')) {
			var alive_targets = [];
			for (var user in users) {
				if (users[user].alive) {alive_targets.push(user);}
			}
			night_target = core.randomElement(alive_targets);
		}

		//slow mafia modifier
		if (hasOption(game_id, 'slow mafia') && games[game_id].page < 3) {
			night_target = 'no one';
		}
		
		//handling people who vote "stay low"
		for (var user in users) {
			for (var card in users[user].acting) {
				if (users[user].acting[card] === 'stay low') {
					
					//handle must-act here
					if (hasCard(user, 'must act')) {
						users[user].acting[card] = core.randomElement(alivePlayers(game_id, user));
					}
					
					//if they are allowed to vote no one..
					else {
						delete users[user].acting[card];
					}
				}
			}
		}
		
		//setting up night kill actions for all mafioso according to the vote
		for (var user in users) {
			if (hasCard(user, 'mafioso') && !hasCard(user, 'lone wolf')) {
				users[user].acting.mafioso = night_target;
			}
		}
		
		//FIRST we handle all actions that can stop a visit from going through
		//mafia roleblockers go first
		for (var user in users) {
			if (hasCard(user, 'mafioso') && users[user].acting.roleblock) {
				users[users[user].acting.roleblock].acting = {};
			}
		}
		//then others roleblockers
		for (var user in users) {
			if (hasCard(user, 'others') && users[user].acting.roleblock) {
				users[users[user].acting.roleblock].acting = {};
			}
		}
		//finally town roleblockers
		for (var user in users) {
			if (hasCard(user, 'townsfolk') && users[user].acting.roleblock) {
				users[users[user].acting.roleblock].acting = {};
			}
		}
		//then  nervous players are stripped of their actions simultaniously if they would have a visti
		var nervous = [];
		for (var user in users) {
			if (hasCard(user, 'nervous')) {
				for (var users_two in users) {
					for (var card in users[users_two].acting)
					if (users[users_two].acting[card] === user) {
						nervous.push(user);
					}
				}
			}
		}
		for (var i in nervous) {users[nervous[i]].acting = {};}
		
		//AT THIS POINT all visits are locked in and confirmed to happen
		//set up object for actions by target {target: {card:[visitor, ..], .. }, ..},
		//actions by card {card: {visitor:target, ..}, ..} If you have multiples of a card, you still only use it once
		var actions_by_target = {};
		var actions_by_card = {};
		var night_victims = [];
		for (var user in users) {
			for (var action in users[user].acting) {
				var target = users[user].acting[action];
				if (!actions_by_target.hasOwnProperty(target)) {actions_by_target[target] = {};}
				if (!actions_by_target[target].hasOwnProperty(action)) {actions_by_target[target][action] = [];}
				actions_by_target[target][action].push(user);
				if (!actions_by_card.hasOwnProperty(action)) {actions_by_card[action] = {};}
				actions_by_card[action][user] = target;
			}
		}

		//increment phase (so that system messages appear on correct day)
		increment_phase(game_id);
		
		//determine leader for mafia kill
		var first_killing_maf = '';var killing_mafs = {'soldier':[], 'normal':[], 'captain':[]};
		if (night_target !== 'no one') {
			if (actions_by_card.mafioso) {
				for (var maf in actions_by_card.mafioso) {
					if (hasCard(maf, 'soldier')) {killing_mafs.soldier.push(maf);}
					else if (hasCard(maf, 'captain')) {killing_mafs.captain.push(maf);}
					else {killing_mafs.normal.push(maf);}
				}
				if (killing_mafs.soldier.length > 0) {
					first_killing_maf = core.randomElement(killing_mafs.soldier);
				} else if (killing_mafs.normal.length > 0) {
					first_killing_maf = core.randomElement(killing_mafs.normal);
				} else {
					first_killing_maf = core.randomElement(killing_mafs.captain);
				}
				night_target = actions_by_card.mafioso[first_killing_maf];//just incase the leading maf gets redirected
			}
		} if (first_killing_maf === '') {night_target = 'no one';}//incase no killing maf could be found
		
		//mafia group kill
		if (night_target !== 'no one') {
			night_kill(actions_by_target, actions_by_card, night_victims, night_target, first_killing_maf);
		}
		
		//vigilante kill
		for (var visitor in actions_by_card['vigilante']) {
			night_kill(actions_by_target, actions_by_card, night_victims, actions_by_card['vigilante'][visitor], visitor);
		}
		
		//execute all kills
		var data = {};
		for (var i in night_victims) {
			var victim = night_victims[i];
			
			games[game_id].users[victim].alive = false;
			
			//revealed role and kill message
			var message;
			var role = '';
			if (hasCard(victim, 'noreveal')) {
				message = {m: victim+' turned up dead this morning!'};
			} else if (actions_by_target[victim] && actions_by_target[victim].gravedigger) {
				message = {m: victim+' missed the town meeting and isnt at home...'};
			} else {
				role = games[game_id].users[victim].role;
				games[game_id].users[victim].reveal = role;
				message = {m: victim+', the '+role+' turned up dead this morning!'};
			}

			//oracle
			if (hasCard(victim, 'oracle')) {
				var reveal_targ = users[victim].data.oracle;
				if (reveal_targ) {
					reveal(reveal_targ, users[reveal_targ].role);
				}
			}
			
			data[victim] = [message, role];
			games[game_id].messages[games[game_id].page].push(message);
		}//now send the message to end the night
		data['@timer'] = activateTimer(game_id, 'day');
		io.to(game_id).emit('night end', data);

		//investigative actions
		for (var visitor in actions_by_card['seer']) {
			var target = actions_by_card['seer'][visitor];
			var role = games[game_id].users[target].role;
			var msg = {m:'You divine the role of'+target+', the '+role+'.', '@':[visitor]};
			io.to('@'+visitor).emit('msg', msg);
			games[game_id].messages[games[game_id].page].push(msg);
		}
		for (var visitor in actions_by_card['investigate']) {
			var target = actions_by_card['investigate'][visitor];
			var alignment = games[game_id].users[target].alignment;
			
			//lawyered
			if (actions_by_target[target].lawyer) {alignment = 'townsfolk';}
			//framed
			if (actions_by_target[target].framer) {alignment = 'mafioso';}
			//godfather
			if (hasCard(target, 'godfather')) {alignment = 'townsfolk';}
			//suspicious
			if (hasCard(target, 'suspicious')) {alignment = 'mafioso';}
			
			var msg = {m:'You discover that '+target+', is a member of the '+alignment+'.', '@':[visitor]};
			io.to('@'+visitor).emit('msg', msg);
			games[game_id].messages[games[game_id].page].push(msg);
		}
		for (var visitor in actions_by_card['follow']) {
			var target = actions_by_card['follow'][visitor];
			var targettargets = [];
			for (var action in games[game_id].users[target].acting) {
				targettargets.push(games[game_id].users[target].acting[action]);
			}
			var m = 'You follow '+target+' throughout the evening as they visit ';
			if (targettargets.length === 0) {m+='no one'} else {m+=targettargets.join(", ");}
			var msg = {'m':m+'.', '@':[visitor]};
			io.to('@'+visitor).emit('msg', msg);
			games[game_id].messages[games[game_id].page].push(msg);
		}
		for (var visitor in actions_by_card['watch']) {
			var target = actions_by_card['watch'][visitor];
			var targetvisitors = [];
			for (var card in actions_by_target[target]) {
				for (i in actions_by_target[target][card]) {
					var newvisitor = actions_by_target[target][card][i];
					if (newvisitor !== visitor && targetvisitors.indexOf(newvisitor) === -1) {targetvisitors.push(newvisitor);}
				}
			}
			var m = 'You watch '+target+' throughout the evening as they are visited by ';
			if (targetvisitors.length === 0) {m+=' no one'} else {m+=targetvisitors.join(", ");}
			var msg = {'m':m+'.', '@':[visitor]};
			io.to('@'+visitor).emit('msg', msg);
			games[game_id].messages[games[game_id].page].push(msg);
		}
		for (var visitor in actions_by_card['oracle']) {
			users[visitor].data.oracle = actions_by_card['oracle'][visitor];
			var msg = {'m':'You are concentrating on '+actions_by_card['oracle'][visitor]+', if you die their role will be revealed!', '@':[visitor]};
			io.to('@'+visitor).emit('msg', msg);
			games[game_id].messages[games[game_id].page].push(msg);
		}
		for (var visitor in actions_by_card['convert']) {
			var target = actions_by_card['convert'][visitor];
			var new_align = games[game_id].users[visitor].alignment;
			var old_align = games[game_id].users[target].alignment;
			games[game_id].users[target].alignment = new_align;
			removeCard(target, old_align);
			addCard(target, new_align);
		}
		for (var visitor in actions_by_card['steal']) {	
			var target = actions_by_card['steal'][visitor];
			for (var card in games[game_id].users[target].cards) {
				if (alignment_cards.indexOf(card) === -1) {
					removeCard(target, card);
					addCard(visitor, card);
				}
			}
			removeCard(visitor, steal);
		}
		
		//finish up
		for (var user in games[game_id].users) {

			//ticking off a slow card
			if (hasCard(user, 'slow')) {removeCard(user, 'slow');}

			//one use cards expire
			if (hasCard(user, 'one use')) {
				for (var card in users[user].acting) {removeCard(user, card);}
			}

			//resetting actions for the following day
			games[game_id].users[user].acting = {};
		}
		
		check_wincons(game_id);
	}
	
	//kill function to be used inside the night action logic to mark players for death
	night_kill = function (actions_by_target, actions_by_card, night_victims, target, killer) {

		//getting some basics
		var game_id = gameusers[target];

		//iron man
		if (hasCard(target, 'ironman')) {}
		//save
		else if (actions_by_target[target] && actions_by_target[target].save) {}
		//bulletproof
		else if (hasCard(target, 'bulletproof')) {removeCard(target, 'bulletproof');}
		//bodyguard. bodyguards cant be guarded
		else if (actions_by_target[target] && actions_by_target[target].bodyguard && !hasCard(target, 'bodyguard')) {
			var bg = core.randomElement(actions_by_target[target].bodyguard);
			night_kill(actions_by_target, actions_by_card, night_victims, bg, killer);
		}
		//successful kill, add to the victims list if not already there
		else if (night_victims.indexOf(target) === -1) {

			//add the target to the kills list
			night_victims.push(target);

		}

		//bomb
		if (hasCard(target, 'bomb') && killer !== '') {night_kill(actions_by_target, actions_by_card, night_victims, killer, '');}
	}

	day_kill = function (target, killer) {
		
		//getting some basics
		var game_id = gameusers[target];

		if (hasCard(target, 'ironman')) {}
		else if (hasCard(target, 'bulletproof')) {removeCard(target, 'bulletproof');}
		else {
			games[game_id].users[target].alive = false;
			games[game_id].users[target].vote = '';
			
			//revealed role and kill message
			var message;
			var role = '';
			if (hasCard(target, 'noreveal')) {
				message = {m: target+' is slain!'};
			}  else {
				role = games[game_id].users[target].role;
				games[game_id].users[target].reveal = role;
				message = {m: target+', the '+role+' is slain!'};
			}

			//oracle
			if (hasCard(target, 'oracle')) {
				var reveal_targ = users[target].data.oracle;
				if (reveal_targ) {
					reveal(reveal_targ, users[reveal_targ].role);
				}
			}
			
			games[game_id].messages[games[game_id].page].push(message);
			io.to(game_id).emit('day kill', {target:target, role:role, message:message});
		}
		if (hasCard(target, 'bomb') && killer !== '') {day_kill(killer, '');}
		else {
			check_day_end(game_id);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	create_game = function(setup, game_rank) {
		//defining the game's unique identifier and incrementing global game counter
		var game_id = serverSession+'-'+String(last_game_id+1);
		last_game_id += 1;

		//finding total_players
		var total_players = 0;
		for (var catagory in setup.catagories) {
			total_players += setup.catagories[catagory][0];
		}
		
		//creating game!
		games[game_id] = {
			'users' : {}, 'phase' : 'pregame', 'page' : 0, 'messages' : {0: []}, 'pregamestarttime' : Math.floor(Date.now() / 1000),
			'setup' : setup, 'total_players':total_players, 'events':{}, 'rank' : game_rank
			};
			
		games_filling[game_rank].push(game_id);
		return game_id;
	}

	get_a_setup = function(rank) {
		//fix this later
		var setup = TEMP_setups[core.randomElement(Object.keys(TEMP_setups))];
		return setup;
	}

	validate_setup = function (setup) {
		//MAKE SURE THERE AREN'T TOO MANY CARDS, CATAGORIES, OR ROLES
		//validating options parameter
		var options = [];
		for (var e in setup.options) {
			if (allowed_options.indexOf(setup.options[e]) !== -1 && !options[e]) {
				options.push(setup.options[e]);
			}
		}

		//validating catagories parameter
		var catagories = {};
		var total_players = 0;
		var role_list = [];
		for (var e in setup.catagories) {
			if (core.niceString(e) && !catagories[e]) {
				catagories[e] = [];
				if (Number(setup.catagories[e][0]) > 0) {
					catagories[e].push(Math.ceil(Number(setup.catagories[e][0])));
					total_players += Math.ceil(Number(setup.catagories[e][0]));
				}
				var role_count = 0;
				for (var i in setup.catagories[e]) {
					if (core.niceString(setup.catagories[e][i]) && i !== 0) {
						role_list.push(setup.catagories[e][i]);
						catagories[e].push(setup.catagories[e][i]);
					}
				}
				if (role_list.length < catagories[e][0]) {return;}
			} else {return;}
		}
		if (total_players > 12 || total_players < 3) {return;}
		
		//validating roles parameter
		var roles = {};
		for (var e in setup.roles) {
			if (role_list.indexOf(e) !== -1 && !roles[e]) {
				roles[e] = [];
				var num_alignment_cards = 0;
				for (i in setup.roles[e]) {
					if (allowed_cards.indexOf(setup.roles[e][i]) !== -1) {
						roles[e].push(setup.roles[e][i]);
						if (alignment_cards.indexOf(setup.roles[e][i]) !== -1) {num_alignment_cards += 1;}
					}
				}
				if (num_alignment_cards !== 1) {return;}
			}
		}

		//validating timers
		var time = {};
		for (var e in setup.time) {
			if (e === 'day' && Number(setup.time[e]) > 0) {
				time.day = Math.max(Math.min(Number(setup.time[e]), 1209600), 30);
			} else if (e === 'night' && Number(setup.time[e]) > 0) {
				time.night = Math.max(Math.min(Number(setup.time[e]), 432000), 15);
			}
		}
		
		//making sure everything is referenced
		for (var e in catagories) {
			for (i in catagories[e]) {
				if (i > 0 && !roles.hasOwnProperty(catagories[e][i])) {
					return;
					}
			}
		}

		return {'options': options, 'catagories':catagories, 'roles':roles, 'time':time};
	}

	look_for_sub = function (user) {
		var game_id = gameusers[user];

		//adding the player to the needed subs list
		if (!needs_sub[game_id]) {
			needs_sub[game_id] = {user : Date.now()};
		} else {
			needs_sub[game_id][user] = Date.now();
		}

	}


	//create a new custom game with the given parameters, and join it afterwards
	app.post('/creategame', function(req, res) {
		
		//user is logged or the session is debug
		if (req.session.username || serverSession === 'debug') {
			
			if (serverSession === 'debug') {var username = 'debugger';}
			else {var username = req.session.username;}
			
			//user is not in a game
			if (!gameusers.hasOwnProperty(username)) {
				
				//initialize valid setup
				var valid_setup = validate_setup({'options': req.body.options, 'catagories':req.body.catagories, 'roles':req.body.roles, 'time':req.body.time});
				
				//if the setup is valid
				if (valid_setup) {
					
					//create the game
					var game_id = create_game(valid_setup, 'custom');
					
					//send the game id back to the creator so they can join it
					res.send(game_id);
				}
				//if setup validation fails..
				else {
					
				}
			}
			//user is already in a game..
			else {
				
			}
		}
		//user isn't logged in
		else {
			
		}
		
	});

	//direct players joining casual or ranked
	app.get('/play/:rank', function(req, res) {
		var username = req.session.username;
		var rank = req.params.rank;

		//user is logged in, or on a guest account & rank valid
		if (username || serverSession === 'debug') {
			if (['casual', 'ranked'].indexOf(rank) !== -1) {

				//if a game needs a sub
				if (Object.keys(needs_sub).length > 0) {

					//find game/user that's been waiting the longest
					var longest_wait_time = Date.now() + 5000;//in the future..
					var longest_wait_user = '';
					for (var game_id in needs_sub) {
						for (var user in needs_sub[game_id]) {
							if (needs_sub[game_id][user] < longest_wait_time) {
								longest_wait_time = needs_sub[game_id][user];
								longest_wait_user = user;
							}
						}
					}

					//if the longest waiting player has been waiting this many ms
					if (longest_wait_time + 60000 < Date.now()) {
						//do the sub regardless (or if theres more than one sub needed, bail the game?)
					}
					//otherwise only do the sub if its a previous offender
					else {
						//database call to determine previous vegges
						models.User.findOne({username: username}, function(err, user) {

							//do the sub and lower the veg karma if the player has some
							if (user.veg_karma > 0) {
								user.veg_karma -= 1
								user.save();
							}
							//don't do the sub if the player has no veg karma
							else {
								var game_id = find_appropriate_game(username, rank, 0);
								res.send({game_id:game_id, join_code:generate_join_code(username, game_id)});
							}
						});
					}
				}
				//game doesn't need a sub
				else {
					var game_id = find_appropriate_game(username, rank, 0);
					res.send({game_id:game_id, join_code:generate_join_code(username, game_id)});
				}
			}
		}
	});
	generate_join_code = function (user, game_id) {
		var join_code = core.random(8, 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789');
		join_codes[join_code] = {user:user, game_id:game_id};
		return join_code;
	}
	initiate_sub = function (user_in, user_out, game_id) {

	}
	find_appropriate_game = function (user, rank, recurs) {
		for (var i in games_filling[rank]) {
			if (filter_prefs(user, games_filling[rank][i]) || recurs > 4) {
				var game_id = games_filling[rank][i];
				return game_id;
			} 
		}
		void create_game(get_a_setup(rank), rank);
		return find_appropriate_game(user, rank, recurs+1);
	}
	filter_prefs = function (user, game_id) {
		//filter user preferences here.. return true for now
		return true;
	}
	
	//joining an existing game, either for the first time or rejoining. Has the game ID.
	app.get('/game/:id', function(req, res) {
		
		//user is logged in, or on a guest account, and a specific game is requested
		if ((req.session.username || serverSession === 'debug') && req.params.id) {//DEBUG GAMEID ENABLED
			
			//the game exists
			if (games.hasOwnProperty(req.params.id)) {
				
				var game_id = req.params.id;
				if (serverSession === 'debug' && !req.session.username) {var username = core.random(8, 'qwertyuioplkjhgfdsazxcvbnm1234567890');}
				else {var username = req.session.username;}
				
				//user is already in the specific game requested, and needs back in
				if (gameusers.hasOwnProperty(username) && gameusers[username] === game_id) {
					
					//let's find it's token
					for (var token in tokens) {
						if (tokens[token] === username) {return_token = token;}
					}
					
					//then put it back into the game
					res.render('pages/game', {
						username: username,
						game_id: game_id,
						token: return_token,
						game: client_game_data(game_id, username)
					});
				}
				//user isn't in a game, let's try to put it in this one
				else if (!gameusers.hasOwnProperty(username)) {

					//make sure they have an appropriate join code if the game isn't custom
					if (games[game_id].rank === 'custom' || (join_codes.hasOwnProperty(req.query.join_code) && join_codes[req.query.join_code].game_id === game_id)) {

						//.. so long as the game hasn't started yet (or is full on pregame)
						if (games[game_id].phase === 'pregame' && games[game_id].total_players !== Object.keys(games[game_id].users).length) {
							var token = core.random(16, 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789');
							
							//adding user into the system
							tokens[token] = username;
							gameusers[username] = game_id;
							games[game_id].users[username] = {
								'vote' : '', 'alive':true, 'acting':{},
								'role' : '', 'cards' : [], 'data':{}
							};

							//setting up join message
							var suffix;
							if (join_codes.hasOwnProperty(req.query.join_code) && join_codes[req.query.join_code].user !== username) {
								suffix = ' has been invited to the game by '+join_codes[req.query.join_code].user+'!';
							} else {suffix = ' has joined the game!';}
							var msg = {m:username+suffix};


							//tell the other players that a new user has joined
							io.to(game_id).emit('user joined game', {username: username, userdata: {'alive':true, 'vote':''}, message:msg});
							
							//prepare to start the game if that's everybody
							if (games[game_id].total_players === Object.keys(games[game_id].users).length) {
								var startgameEvent = setTimeout(function () {
									startgame(game_id);
								}, 5000);
								games[game_id].events.startgame = startgameEvent;
								games[game_id].endtime = Date.now() + 5000;
							}
							//give the user their new page in the pregame
							res.render('pages/game', {
								username: username,
								game_id: game_id,
								token: token,
								game: client_game_data(game_id, username)
							});
						}
						//if it has started, the user can be a spectator and possible sub
						else {
							
						}
					}
					//if it's ranked/casual but there's no good join code
					else {

					}
				}
				//user is in a different game, let's handle that
				else {
				
				}
			}
			//or maybe the game doesn't exist?
			else {
				
			}
		}
		//user isn't logged in, or there's no game specified
		else {
			
		}
	});
	
	//GAME SOCKET CONNECTION
	io.on('connection', function (socket) {
		console.log('a player has connected')
		
		socket.on('game handshake', function (data) {
			if (tokens.hasOwnProperty(data.token)) {
				var username = tokens[data.token];
				var game_id = gameusers[username];
				socket.username = username;
				socket.join(game_id);
				socket.join('@'+username);
				usersockets[username] = socket;
				if (hasCard(username, 'mafioso') && !hasCard(username, 'lone wolf')) {
					socket.join(game_id+':mafia');
				}
				else if (hasCard(username, 'others') && !hasCard(username, 'lone wolf')) {
					socket.join(game_id+':others');
				}
				else if (hasCard(username, 'mason') && !hasCard(username, 'lone wolf')) {
					socket.join(game_id+':mason');
				}
			}
		});
		
		socket.on('msg', function (data) {
			if (tokens.hasOwnProperty(data.token)) {
				var username = tokens[data.token];
				var game_id = gameusers[username];
				
				//work with message data
				var content = data.content.toString();
				var game_time = Math.floor(Date.now() / 1000) - games[game_id]['pregamestarttime'];
				var msg_data = {'t':game_time, 'u':username, 'm':content};
				
				//if it's a quote
				if (data.quote) {
					var quoted_player = data.quote.toString();
					var quote_page = data.page.toString();
					//check if user is in the same game
					if (gameusers[quoted_player] && gameusers[quoted_player] === gameusers[username]) {
						//make sure the message is real
						for (var msg in games[game_id].messages[quote_page].page) {
							if (msg.m === content && msg.u === quoted_player) {
								var msg_valid = true;
								break;
							}
						}

						if (msg_valid) {
							msg_data.q = quoted_player;
						}
					}
				}

				//if the user is alive
				if (isAlive(username)) {
				
					//validate a pregame or day time message
					if (games[game_id].phase === 'pregame' || games[game_id].phase === 'day') {
						games[game_id].messages[games[game_id].page].push(msg_data);
						io.to(game_id).emit('msg', msg_data);
					}
					//validate a night time message
					else if (games[game_id].phase === 'night') {
						msg_data['@'] = [];
						
						//mafia night meeting chat
						if (hasCard(username, 'mafioso')) {
							for (var user in games[game_id].users) {if (hasCard(user, 'mafioso') && !hasCard(user, 'lone wolf')) {
								msg_data['@'].push(user);}}
							io.to(game_id+':mafia').emit('msg', msg_data);}
						//others night meeting chat
						if (hasCard(username, 'others')) {
							for (var user in games[game_id].users) {if (hasCard(user, 'others') && !hasCard(user, 'lone wolf')) {
								msg_data['@'].push(user);}}
							io.to(game_id+':others').emit('msg', msg_data);}
						//mason night meeting chat
						if (hasCard(username, 'mason')) {
							for (var user in games[game_id].users) {if (hasCard(user, 'mason') && !hasCard(user, 'lone wolf')) {
								msg_data['@'].push(user);}}
							io.to(game_id+':mason').emit('msg', msg_data);}
						games[game_id].messages[games[game_id].page].push(msg_data);
					}
				
				}
				//if the user is dead
				else {
					
				}
			}
		});
		
		socket.on('vote', function (data) {
			if (tokens.hasOwnProperty(data.token)) {
				var username = tokens[data.token];
				var game_id = gameusers[username];
				
				//if the target player is in the same game (or unvote or no one)
				if (gameusers[data.target] === game_id || data.target === '' || data.target === 'no one') {
					
					//the target is no one or alive & the voter isn't dead
					if (isAlive(username) && (isAlive(data.target) || data.target === '' || data.target === 'no one')) {
						var message = {m:username+' votes '+data.target};
						if (data.target === '') {message.m = username+' unvotes';}
						
						//day time vote
						if (games[game_id].phase === 'day') {
							games[game_id].users[username].vote = data.target;
							io.to(game_id).emit('vote change', {voter: username, voted: data.target, message: message});
							games[game_id].messages[games[game_id].page].push(message);
							check_day_end(game_id);
						}
						//night time vote
						else if (games[game_id].phase === 'night' && hasCard(username, 'mafioso') && !hasCard(username, 'lone wolf')) {
							games[game_id].users[username].vote = data.target;
							io.to(game_id+':mafia').emit('vote change', {voter: username, voted: data.target, message: message});
							message['@'] = [];
							for (var user in games[game_id].users) {
								if (hasCard(user, 'mafioso') && !hasCard(user, 'lone wolf')) {message['@'].push(user);}
							}
							games[game_id].messages[games[game_id].page].push(message);
							check_night_end(game_id);
						}
					}
				}
			}
		});
		
		socket.on('action', function (data) {
			if (tokens.hasOwnProperty(data.token)) {
				var username = tokens[data.token];
				var game_id = gameusers[username];
				var card = data.card;
				//if you have the card to begin with..
				if (hasCard(username,card)) {
					
					//if the target player is in the same game
					if (gameusers[data.target] === game_id || data.target === 'stay low') {
						
						//the target is no one or alive & the acter isn't dead & the target isn't the actor
						if ((isAlive(data.target) || data.target === 'stay low') && isAlive(username) && username !== data.target) {
							
							//if the player is allowed to act by special cards
							if (isAllowedToAct(username)) {

								//day time action
								if (games[game_id].phase === 'day' && dayactions.indexOf(card) !== -1) {
									
									//mayor
									if (card === 'mayor') {
										reveal(username, 'mayor');
										var message = {m:'BEHOLD! '+username+' is the town mayor!'};
										games[game_id].messages[games[game_id].page].push(message);
										io.to(game_id).emit('msg', message);
									}
									//gun and knife
									else if (card === 'gun' || card === 'knife') {

										//handling the message
										if (card === 'gun') {
											var message = {m:username+' pulls out a gun in the middle of the crowd and shoots '+data.target};
											var message = {m:data.target+' is attacked with a knife!'};
										}
										games[game_id].messages[games[game_id].page].push(message);
										io.to(game_id).emit('msg', message);
										removeCard(username, card);

										//the actual kill
										day_kill(data.target, username);

									}


								}
								//night time action
								else if (games[game_id].phase === 'night' && nightactions.indexOf(card) !== -1) {

									//if it's the same target
									if (games[game_id].users[username].acting[card] && games[game_id].users[username].acting[card] === data.target) {
										delete games[game_id].users[username].acting[card];
										var msg = {m:'You rethink your decision..', '@':[username]};
									}
									//if the target is no one
									else if (data.target === 'stay low') {
										games[game_id].users[username].acting[card] = 'stay low';
										var msg = {m:'You decide to stay low tonight and not use your '+card+' power.', '@':[username]};
									}
									//if it's a different/new target
									else {
										games[game_id].users[username].acting[card] = data.target;
										var msg = {m:'You will be visiting '+data.target+' tonight, and using your '+card+' power.', '@':[username]};
									}
									games[game_id].messages[games[game_id].page].push(msg);
									io.to('@'+username).emit('action confirmed', {card: card, target: data.target, message:msg});
									check_night_end(game_id);

								}
							}
						}
						//if the target or player is dead or trying to target itself!
						else {
							
						}
					}
				}
			}
		});
		
		socket.on('disconnect', function () {
			//disconnection events
			console.log('a player disconnected');
		});

	});

}