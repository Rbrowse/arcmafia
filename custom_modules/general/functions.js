module.exports.random = function (howMany, chars) {
	
	chars = chars 
		|| "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	var rnd = crypto.randomBytes(howMany)
		, value = new Array(howMany)
		, len = chars.length;

	for (var i = 0; i < howMany; i++) {
		value[i] = chars[rnd[i] % len]
	};

	return value.join('');
	
}

module.exports.niceString = function (s) {
	var problem_response = false;
	if (typeof s === 'string') {
		for (c in s) {
				if ('qwertyuiopasdfghjklzxcvbnm1234567890- QWERTYUIOPASDFGHJKLZXCVBNM'.indexOf(s[c]) === -1) {problem_response = true;}
			}
		if (!problem_response && s.length < 16 && s.length > 1) {return true;} else {return false;}
	}
}

module.exports.shuffleArray = function (array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

module.exports.randomElement = function (array) {
	return array[Math.floor(Math.random() * (array.length))];
}

module.exports.dumpError = function (err) {
	if (typeof err === 'object') {
		if (err.message) {
			console.log('\nMessage: ' + err.message)
		}
		if (err.stack) {
			console.log('\nStacktrace:')
			console.log('====================')
			console.log(err.stack);
		}
	} else {
    console.log('dumpError :: argument is not an object');
	}
}