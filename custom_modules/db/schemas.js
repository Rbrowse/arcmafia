/* global mongoose */

var Schema = mongoose.Schema;

var userSchema = new Schema({
	username: {type: String, required: true, unique: true},
	username_lower: {type: String, required: true, unique: true},
	email: {type: String, required: true, unique: true},
	pass: {type: String, required: true},
	ip: {type: String, required: true},
	time: {type: Number, required: true},
	auth_status: {type: Number, required: true},
	veg_karma: {type: Number, required: false}
});

module.exports = {
	user: userSchema
};