/* global schemas */
/* global mongoose */

var User = mongoose.model('User', schemas.user);

module.exports = {
	'User': User
};