/* global app */

module.exports = function () {
	
	app.get('/', function(req, res) {
		if (req.session.username) {
			res.render('pages/indexloggedin', {
				username: req.session.username
			});
		}
		else {
			res.render('pages/indexloggedout', {
				username: 'logged out (temp)'
			});
		}
	});
	
	app.get('/tou', function(req, res) {
		res.render('pages/tou');
	});
	
	app.get('/logout', function(req, res) {
		req.session.username = null;
		res.render('pages/indexloggedout', {
			username: 'logged out (temp)'
		});
	});

}