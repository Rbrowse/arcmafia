/* global handleError */
/* global models */
/* global app */

module.exports = function () {
	
	app.post('/register', function(req, res) {
		
		// this variable... is important..
		var problem_response = false;

		// getting input values
		var ip = get_ip(req)['clientIp'];
		var username = req.body.username;
		var username_lower = username.toLowerCase();
		var email = req.body.email;
		var pass = req.body.pass;
		
		// checking email validation
		if (!validator.validate(email)) {problem_response = true;}
		if (email.length > 60) {problem_response = true;}
		
		// checking password validation
		for (c in pass.toLowerCase()) {
			if ('qwertyuiopasdfghjklzxcvbnm1234567890,?=+(){}[]<>?/|*:;~`!@#$%^& -_.'.indexOf(pass.toLowerCase()[c]) == -1) {problem_response = true;}
		}
		if (pass.length < 6) {problem_response = true;}
		if (pass.length > 300) {problem_response = true;}
		
		// checking username validation
		for (c in username_lower) {
			if ('1234567890-_.qwertyuiopasdfghjklzxcvbnm'.indexOf(username.toLowerCase()[c]) == -1) {problem_response = true;}
		}
		if (username.length < 4) {problem_response = true;}
		if (username.length > 16) {problem_response = true;}
		
		// checking ip, registering acc in callback
		models.User.find({ip: ip}, function(err, data) {
			
			// time for the creation of the account
			var d = new Date();
			var n = d.getTime();
			var error_text = 'There was a problem registering your account!';
			
			if (data.length > 0) {
				for (i in data) {
					// if the same ip has been used in the last hour, its a problem
					if (data[i].time + 3600000 > n) {
						problem_response = true;
						error_text = 'This network has already made an account recently! Try again in an hour.';
					}
				}
			}
			if (!problem_response) {
				
				// entering data into the database
				models.User.create({
					username: username,
					email: email.toLowerCase(),
					pass: pass,
					username_lower: username_lower,
					ip: ip,
					time: n,
					auth_status: 0
					
				}, function(err) {
					if (err) {
						// error from database validation
						res.send('database error');
					}
					else {
						console.log(username + " has been successfully registered!");
						req.session.username = username;
						res.send('1');
					}
				});
			} else {res.send(error_text);}// error from custom validation
		});	
	});
	
	app.post('/login', function(req, res) {
		if (!req.session.username) {
			
			// getting input values
			var name = req.body.loginname;
			name = name.toLowerCase();
			var pass = req.body.pass;
			var searchstring;
			
			if (name.indexOf('@') == -1) {
				searchstring = {username_lower: name};
			} else {
				searchstring = {email: name};
			}
			
			models.User.find(searchstring, function(err, data) {
				if (data.length > 0) {
					var userInfo = data[0];
					
					// is password correct? is it banned?
					if (userInfo.pass == pass && userInfo.auth_status > -1) {
						
						// login successful
						console.log(userInfo.username + " logged in!");
						req.session.username = userInfo.username;
						res.send('1');
						
					} else {
						// banned users have no rights
						res.send('Incorrect password.');
					}
				} else {
					res.send('User does not exist.');
				}
			});
		} else {
			// if you're already logged in
			res.redirect('/');
		}
	});
	
	app.post('/check_username', function(req, res) {
		// getting input values
		var name = req.body.username;
		var name_lower = name.toLowerCase();
		
		models.User.find({username_lower: name_lower}, function(err, data) {
			if (data.length > 0) {
				res.send('taken');
			} else {
				res.send('available');
			}
		});
	});
	
	app.post('/check_email', function(req, res) {
		// getting input values
		var email = req.body.email;
		email = email.toLowerCase();
		
		models.User.find({email: email}, function(err, data) {
			if (data.length > 0) {
				res.send('taken');
			} else {
				res.send('available');
			}
		});
	});
	
	// ADD MORE POSTS HERE
}