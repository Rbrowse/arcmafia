/* global app */

module.exports = function () {
	
	io.on('connection', function (socket) {
		
		/*
		EMIT CODES:
			'prompt', {content: String}
			'exe', {script : String}
			
		ON CODES:
			'handshake'
		*/
		
		socket.emit('prompt', {content: 'Socket connection established.'});
		
		socket.on('handshake', function (data) {
			socket.emit('prompt', {content: 'Handshake completed.'});
		});
		
		socket.on('vote', function (data) {
			var res = game_vote(data.token, data.target, data.vote_type);
			socket.emit('prompt', {content: 'Handshake completed.'});
		});
		
		socket.on('disconnect', function () {
			//disconnection events
		});

	});

}